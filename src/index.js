import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

import { composeWithDevTools } from "redux-devtools-extension";
import monitorReducerEnhancer from "./redux/enhancers/monitorReducers";

import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import { createLogger } from "redux-logger";

import { preloadedState } from "./redux/preloadedState";
import { rootReducer } from "./redux/reducers";
import { loadState, saveState } from "./redux/localStorage";

const persistedState = Object.assign(
  {},
  { ...preloadedState },
  { ...loadState() }
);

const middleware = [createLogger()];

const middlewareEnhancer = applyMiddleware(...middleware);

const enhancers = [middlewareEnhancer, monitorReducerEnhancer];
const composedEnhancers = composeWithDevTools(...enhancers);

const store = createStore(rootReducer, persistedState, composedEnhancers);

store.subscribe(() => {
  saveState({
    loggedIn: store.getState().loggedIn,
    tableBackground: store.getState().tableBackground,
    favorites: store.getState().favorites,
  });
});

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
