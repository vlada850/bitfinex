import { combineReducers } from "redux";

import {
  SET_INITIAL_SYMBOLS,
  SET_LOGGED_IN,
  SET_INITIAL_SYMBOL_DATA,
  SET_TABLE_BACKGROUND,
  SET_SYMBOL_CLICKED,
  SET_FAVORITES,
  REMOVE_FROM_FAVORITES,
  SET_FAVORITE_SYMBOL_DATA,
} from "./actions";

const initialSymbols = (state = [], action) => {
  switch (action.type) {
    case SET_INITIAL_SYMBOLS:
      return action.data.slice(0, 5).map((el) => el.toUpperCase());
    default:
      return state;
  }
};

const tableBackground = (state = "table-space", action) => {
  switch (action.type) {
    case SET_TABLE_BACKGROUND:
      return action.data;
    default:
      return state;
  }
};

const loggedIn = (state = false, action) => {
  switch (action.type) {
    case SET_LOGGED_IN:
      return true;
    default:
      return state;
  }
};

const initialSymbolData = (state = [], action) => {
  switch (action.type) {
    case SET_INITIAL_SYMBOL_DATA:
      return [
        ...state,
        {
          symbol: action.data.symbol,
          lastPrice:
            action.data.data[6] > 1
              ? action.data.data[6].toFixed(2)
              : action.data.data[6],
          dailyChange:
            action.data.data[4] > 1
              ? action.data.data[4].toFixed(2)
              : action.data.data[4],
          dailyChangeRelative:
            action.data.data[5] > 0
              ? `+${action.data.data[5]}`
              : action.data.data[5] > 1
              ? action.data.data[5].toFixed(2)
              : action.data.data[5],
          high:
            action.data.data[8] > 1
              ? action.data.data[8].toFixed(2)
              : action.data.data[8],
          low:
            action.data.data[9] > 1
              ? action.data.data[9].toFixed(2)
              : action.data.data[9],
        },
      ];
    default:
      return state;
  }
};

const symbolClicked = (state = false, action) => {
  switch (action.type) {
    case SET_SYMBOL_CLICKED:
      return action.data;
    default:
      return state;
  }
};

const favorites = (state = [], action) => {
  switch (action.type) {
    case SET_FAVORITES:
      return state.includes(action.data) ? state : [...state, action.data];
    case REMOVE_FROM_FAVORITES:
      return state.filter((el) => el !== action.data);
    default:
      return state;
  }
};

const favoriteSymbolsData = (state = [], action) => {
  switch (action.type) {
    case SET_FAVORITE_SYMBOL_DATA:
      return [
        ...state.filter((el) => el.symbol !== action.data.symbol),
        {
          symbol: action.data.symbol,
          lastPrice:
            action.data.data[6] > 1
              ? action.data.data[6].toFixed(2)
              : action.data.data[6],
          dailyChange:
            action.data.data[4] > 1
              ? action.data.data[4].toFixed(2)
              : action.data.data[4],
          dailyChangeRelative:
            action.data.data[5] > 0
              ? `+${action.data.data[5]}`
              : action.data.data[5] > 1
              ? action.data.data[5].toFixed(2)
              : action.data.data[5],
          high:
            action.data.data[8] > 1
              ? action.data.data[8].toFixed(2)
              : action.data.data[8],
          low:
            action.data.data[9] > 1
              ? action.data.data[9].toFixed(2)
              : action.data.data[9],
        },
      ];
    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  initialSymbols,
  tableBackground,
  loggedIn,
  initialSymbolData,
  symbolClicked,
  favorites,
  favoriteSymbolsData,
});
