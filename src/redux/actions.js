export const SET_INITIAL_SYMBOLS = "SET_INITIAL_SYMBOLS";
export const SET_TABLE_BACKGROUND = "SET_TABLE_BACKGROUND";
export const SET_LOGGED_IN = "SET_LOGGED_IN";
export const SET_INITIAL_SYMBOL_DATA = "SET_INITIAL_SYMBOL_DATA";
export const SET_SYMBOL_CLICKED = "SET_SYMBOL_CLICKED";
export const SET_FAVORITES = "SET_FAVORITES";
export const REMOVE_FROM_FAVORITES = "REMOVE_FROM_FAVORITES";
export const SET_FAVORITE_SYMBOL_DATA = "SET_FAVORITE_SYMBOL_DATA";

export const setInitialSymbols = (data) => ({
  type: SET_INITIAL_SYMBOLS,
  data,
});

export const setTableBackground = (data) => ({
  type: SET_TABLE_BACKGROUND,
  data,
});

export const setloggedIn = (data) => ({
  type: SET_LOGGED_IN,
  data,
});

export const setInitialSymbolData = (data) => ({
  type: SET_INITIAL_SYMBOL_DATA,
  data,
});

export const setSymbolClicked = (data) => ({
  type: SET_SYMBOL_CLICKED,
  data,
});

export const setFavorites = (data) => ({
  type: SET_FAVORITES,
  data,
});

export const removeFromFavorites = (data) => ({
  type: REMOVE_FROM_FAVORITES,
  data,
});

export const setFavoriteSymbolData = (data) => ({
  type: SET_FAVORITE_SYMBOL_DATA,
  data,
});
