import { FETCH_SYMBOLS_URL } from "../consts/consts";
import { setInitialSymbols } from "../redux/actions";

export const fetchSymbols = (dispatch) => {
  fetch(`${FETCH_SYMBOLS_URL}`, {
    method: "GET",
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    },
  })
    .then((res) => res.json())
    .then((data) => {
      Array.isArray(data) && dispatch(setInitialSymbols(data));
    })
    .catch((err) => {
      console.log(err);
    });
};
