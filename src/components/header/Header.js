import { setloggedIn, setSymbolClicked } from "../../redux/actions";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const Header = () => {
  const dispatch = useDispatch();
  const loggedIn = useSelector((state) => state.loggedIn);
  const history = useHistory();
  const pathname = window.location.pathname;

  return (
    <header className="header">
      <div className="header-home-favorites">
        <div
          className={pathname === "/" ? "active" : ""}
          onClick={() => {
            history.push("/");
            dispatch(setSymbolClicked(false));
          }}
        >
          HOME
        </div>
        {loggedIn && (
          <div
            className={pathname === "/favorites" ? "active" : ""}
            onClick={() => {
              history.push("/favorites");
              dispatch(setSymbolClicked(false));
            }}
          >
            FAVORITES
          </div>
        )}
      </div>
      <div className="teletrader">TeleTrader</div>
      {!loggedIn ? (
        <button
          className="header-login"
          onClick={() => dispatch(setloggedIn())}
        >
          LOGIN
        </button>
      ) : (
        <div className="welcome-guest">Welcome Guest</div>
      )}
    </header>
  );
};

export default Header;
