import Loader from "../home/loader/Loader";
import { useSelector } from "react-redux";
import Thead from "../thead/Thead";
import Header from "../header/Header";
import TableBackground from "../tableBackground/TableBackground";
import AddRemoveFavoriteButtons from "../buttons/AddRemoveFavoriteButtons";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";

const DetailsPage = () => {
  const history = useHistory();
  const symbolClicked = useSelector((state) => state.symbolClicked);
  const tableBackground = useSelector((state) => state.tableBackground);

  useEffect(() => {
    !symbolClicked && history.push("/");
  }, [symbolClicked, history]);

  return (
    <div className="wrapper">
      <Header />
      <main>
        <table className={tableBackground}>
          <Thead />
          <tbody>
            <tr>
              <td>{symbolClicked ? symbolClicked.symbol : <Loader />}</td>
              <td>{symbolClicked ? symbolClicked.lastPrice : <Loader />}</td>
              <td>{symbolClicked ? symbolClicked.dailyChange : <Loader />}</td>
              <td>
                {symbolClicked ? (
                  `${symbolClicked.dailyChangeRelative}%`
                ) : (
                  <Loader />
                )}
              </td>
              <td>{symbolClicked ? symbolClicked.high : <Loader />}</td>
              <td>{symbolClicked ? symbolClicked.low : <Loader />}</td>
            </tr>
          </tbody>
        </table>
        <AddRemoveFavoriteButtons />
        <TableBackground />
      </main>
    </div>
  );
};

export default DetailsPage;
