import "./favoritesTableRow.css";
import { useEffect, useState } from "react";
import { WEB_SOCKET_API } from "../../../../consts/consts";
import {
  setFavoriteSymbolData,
  setSymbolClicked,
} from "../../../../redux/actions";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../../../home/loader/Loader";
import { useHistory } from "react-router-dom";

const FavoritesTableRow = ({ symbol }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const favoriteSymbolData = useSelector(
    (state) => state.favoriteSymbolsData
  ).find((el) => el.symbol === symbol);
  const [dailyChangeRel, setDailyChangeRel] = useState([]);
  const [dailyChangeRelTrigger, setDailyChangeRelTrigger] = useState(false);

  useEffect(() => {
    let timeout;
    let ws = new WebSocket(`${WEB_SOCKET_API}`);

    ws.onopen = () => {
      ws.send(
        JSON.stringify({
          event: "subscribe",
          channel: "ticker",
          symbol: `t${symbol}`,
        })
      );
    };

    ws.onmessage = (msg) => {
      let response = JSON.parse(msg.data)[1];
      if (Array.isArray(response)) {
        dispatch(
          setFavoriteSymbolData({
            symbol: symbol,
            data: response,
          })
        );
        setDailyChangeRel((prevState) =>
          prevState.length < 2
            ? [...prevState, response[5]]
            : [prevState.slice(1), response[5]]
        );
        setDailyChangeRelTrigger(true);
        timeout = setTimeout(() => setDailyChangeRelTrigger(false), 1000);
      }
    };

    ws.onerror = (err) => {
      ws.close();
      ws = new WebSocket(`${WEB_SOCKET_API}`);
    };

    return () => {
      ws.close();
      clearTimeout(timeout);
    };
  }, [dispatch, symbol]);

  return (
    <tr
      onClick={() => {
        if (favoriteSymbolData) {
          dispatch(setSymbolClicked(favoriteSymbolData));
          history.push("/details");
        }
      }}
    >
      <td>{symbol}</td>
      <td>{favoriteSymbolData ? favoriteSymbolData.lastPrice : <Loader />}</td>
      <td>
        {favoriteSymbolData ? favoriteSymbolData.dailyChange : <Loader />}
      </td>
      <td
        className={
          dailyChangeRel.length === 1
            ? dailyChangeRel[0] < 0
              ? "color-red"
              : "color-green"
            : !dailyChangeRelTrigger
            ? dailyChangeRel[1] < 0
              ? "color-red"
              : "color-green"
            : dailyChangeRel[1] > dailyChangeRel[0]
            ? "bg-color-green"
            : dailyChangeRel[1] < dailyChangeRel[0]
            ? "bg-color-red"
            : dailyChangeRel[1] < 0
            ? "color-red"
            : "color-green"
        }
      >
        {dailyChangeRel.length === 0 ? (
          <Loader />
        ) : favoriteSymbolData ? (
          `${favoriteSymbolData.dailyChangeRelative}%`
        ) : (
          <Loader />
        )}
      </td>
      <td>{favoriteSymbolData ? favoriteSymbolData.high : <Loader />}</td>
      <td>{favoriteSymbolData ? favoriteSymbolData.low : <Loader />}</td>
    </tr>
  );
};

export default FavoritesTableRow;
