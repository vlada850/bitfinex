import "./favoritesTable.css";
import Thead from "../../thead/Thead";
import { useSelector } from "react-redux";

import FavoritesTableRow from "./favoritesTableRow/FavoritesTableRow";
import DetailsPage from "../../detailsPage/DetailsPage";

const FavoritesTable = () => {
  const tableBackground = useSelector((state) => state.tableBackground);
  const symbolClicked = useSelector((state) => state.symbolClicked);
  const favorites = useSelector((state) => state.favorites);

  return (
    <>
      <table className={tableBackground}>
        <Thead />
        <tbody>
          {!symbolClicked ? (
            favorites.map((el, i) => <FavoritesTableRow key={i} symbol={el} />)
          ) : (
            <DetailsPage />
          )}
        </tbody>
      </table>
    </>
  );
};

export default FavoritesTable;
