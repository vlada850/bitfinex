import "./favorites.css";

import FavoritesTable from "./favoritesTable/FavoritesTable";
import TableBackground from "../tableBackground/TableBackground";
import Header from "../header/Header";

const Favorites = () => (
  <div className="wrapper">
    <Header />
    <main>
      <FavoritesTable />
      <TableBackground />
    </main>
  </div>
);

export default Favorites;
