import "./homeTableRow.css";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { WEB_SOCKET_API } from "../../../../consts/consts";
import {
  setInitialSymbolData,
  setSymbolClicked,
} from "../../../../redux/actions";

import Loader from "../../loader/Loader";
import { useHistory } from "react-router-dom";

const HomeTableRow = ({ symbol }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const initialSymbolData = useSelector(
    (state) => state.initialSymbolData
  ).find((el) => el.symbol === symbol);

  useEffect(() => {
    if (initialSymbolData === undefined) {
      let ws = new WebSocket(`${WEB_SOCKET_API}`);

      ws.onopen = () => {
        ws.send(
          JSON.stringify({
            event: "subscribe",
            channel: "ticker",
            symbol: `t${symbol}`,
          })
        );
      };

      ws.onmessage = (msg) => {
        let response = JSON.parse(msg.data)[1];
        if (Array.isArray(response)) {
          dispatch(
            setInitialSymbolData({
              symbol: symbol,
              data: response,
            })
          );
          ws.close();
        }
      };

      ws.onerror = (err) => {
        ws.close();
        ws = new WebSocket(`${WEB_SOCKET_API}`);
      };
    }
  }, [dispatch, symbol, initialSymbolData]);

  return (
    <tr
      onClick={() => {
        if (initialSymbolData) {
          dispatch(setSymbolClicked(initialSymbolData));
          history.push("/details");
        }
      }}
    >
      <td>{symbol}</td>
      <td>{initialSymbolData ? initialSymbolData.lastPrice : <Loader />}</td>
      <td>{initialSymbolData ? initialSymbolData.dailyChange : <Loader />}</td>
      <td>
        {initialSymbolData ? (
          `${initialSymbolData.dailyChangeRelative}%`
        ) : (
          <Loader />
        )}
      </td>
      <td>{initialSymbolData ? initialSymbolData.high : <Loader />}</td>
      <td>{initialSymbolData ? initialSymbolData.low : <Loader />}</td>
    </tr>
  );
};

export default HomeTableRow;
