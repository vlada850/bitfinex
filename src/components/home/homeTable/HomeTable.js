import "./homeTable.css";

import HomeTableRow from "./homeTableRow/HomeTableRow";
import Thead from "../../thead/Thead";
import DetailsPage from "../../detailsPage/DetailsPage";
import { useSelector } from "react-redux";

const HomeTable = () => {
  const initialSymbols = useSelector((state) => state.initialSymbols);
  const tableBackground = useSelector((state) => state.tableBackground);
  const symbolClicked = useSelector((state) => state.symbolClicked);

  return (
    <>
      <table className={tableBackground}>
        <Thead />
        <tbody>
          {!symbolClicked ? (
            initialSymbols.length !== 0 &&
            initialSymbols.map((el, i) => <HomeTableRow key={i} symbol={el} />)
          ) : (
            <DetailsPage />
          )}
        </tbody>
      </table>
    </>
  );
};

export default HomeTable;
