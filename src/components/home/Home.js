import "./home.css";

import HomeTable from "./homeTable/HomeTable";
import TableBackground from "../tableBackground/TableBackground";
import Header from "../header/Header";

const Home = () => (
  <div className="wrapper">
    <Header />
    <main>
      <HomeTable />
      <TableBackground />
    </main>
  </div>
);

export default Home;
