const Thead = () => (
  <thead>
    <tr>
      <td>Name</td>
      <td>Last</td>
      <td>Change</td>
      <td>Change Percent</td>
      <td>High</td>
      <td>Low</td>
    </tr>
  </thead>
);

export default Thead;
