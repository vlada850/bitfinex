import "./tableBackground.css";
import { setTableBackground } from "../../redux/actions";
import { useDispatch, useSelector } from "react-redux";

const TableBackground = () => {
  const dispatch = useDispatch();
  const tableBackground = useSelector((state) => state.tableBackground);
  return (
    <div className="checkbox-wrapper">
      <span>Table Background</span>
      <div className="checkbox-label-wrapper">
        <label>
          <input
            type="checkbox"
            checked={tableBackground === "table-space"}
            onChange={() => dispatch(setTableBackground("table-space"))}
          />
          Space
        </label>
        <label>
          <input
            type="checkbox"
            checked={tableBackground === "table-tropic"}
            onChange={() => dispatch(setTableBackground("table-tropic"))}
          />
          Tropic
        </label>
        <label>
          <input
            type="checkbox"
            checked={tableBackground === ""}
            onChange={() => dispatch(setTableBackground(""))}
          />
          None
        </label>
      </div>
    </div>
  );
};

export default TableBackground;
