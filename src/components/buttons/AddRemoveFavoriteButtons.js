import "./buttons.css";
import { removeFromFavorites, setFavorites } from "../../redux/actions";
import { useDispatch, useSelector } from "react-redux";

const AddRemoveFavoriteButtons = () => {
  const dispatch = useDispatch();
  const loggedIn = useSelector((state) => state.loggedIn);
  const favorites = useSelector((state) => state.favorites);
  const symbolClicked = useSelector((state) => state.symbolClicked);

  return (
    <>
      {loggedIn && symbolClicked && !favorites.includes(symbolClicked.symbol) && (
        <button
          className="add-to-favorites-button"
          onClick={() => dispatch(setFavorites(symbolClicked.symbol))}
        >
          Add to favorites
        </button>
      )}
      {loggedIn && symbolClicked && favorites.includes(symbolClicked.symbol) && (
        <button
          className="remove-from-favorites-button"
          onClick={() => dispatch(removeFromFavorites(symbolClicked.symbol))}
        >
          Remove from favorites
        </button>
      )}
    </>
  );
};

export default AddRemoveFavoriteButtons;
