import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchSymbols } from "./methods/fetchMethods";

import Home from "./components/home/Home";
import Favorites from "./components/favorites/Favorites";
import { useEffect } from "react";
import DetailsPage from "./components/detailsPage/DetailsPage";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    fetchSymbols(dispatch);
  }, [dispatch]);

  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/favorites" component={Favorites} />
        <Route path="/details" component={DetailsPage} />
      </Switch>
    </Router>
  );
}

export default App;
